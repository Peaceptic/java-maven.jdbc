package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class Main extends Application{
    @Override
    public void start(Stage stage) throws Exception {
        try {
            stage.setTitle("Vereinverwaltung");

            AnchorPane root = FXMLLoader.load(getClass().getResource("view/VereinverwaltungUIView.fxml"));

            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            System.out.println("Error: ");
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        launch(args);
    }
}
