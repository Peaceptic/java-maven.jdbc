package application.controller;

import application.ClubMember;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class VereinverwaltungUIController {
    @FXML
    private TextField txtVorname;
    @FXML
    private TextField txtNachname;
    @FXML
    private TextField txtAlter;
    @FXML
    private TableView<ClubMember> tableClubMembers;
    @FXML
    private TableColumn<ClubMember, String> colVorname;
    @FXML
    private TableColumn<ClubMember, String> colNachname;
    @FXML
    private TableColumn<ClubMember, Integer> colAlter;

    ObservableList<ClubMember> clubmembers = FXCollections.observableArrayList();

    application.DatabaseConnectionManager dcm = new application.DatabaseConnectionManager("localhost", "vereinverwaltung", "postgres", "postgre");
    @FXML
    private void clickButton(){
        String firstname = txtVorname.getText();
        String lastname =  txtNachname.getText();
        int age = Integer.parseInt(txtAlter.getText());

        String insertStatement;
        try{
            Connection connection = dcm.getConnection();
            Statement statement = connection.createStatement();
            int result = statement.executeUpdate("INSERT INTO mitglieder(vorname, nachname, alter) VALUES('" + firstname + "','" + lastname + "'," +  age  + ")");
            connection.close();
            System.out.println("Erfolgreich");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    private void clickBtnDelete(){
        String firstname = txtVorname.getText();
        String lastname =  txtNachname.getText();
        int age = Integer.parseInt(txtAlter.getText());

        String insertStatement;
        try{
            Connection connection = dcm.getConnection();
            Statement statement = connection.createStatement();
            int result = statement.executeUpdate("DELETE FROM mitglieder WHERE vorname = " + "'" + firstname + "'" + " AND nachname = " + "'" + lastname + "'" +  "AND alter = " + age);
            connection.close();
            System.out.println("Erfolgreich");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    private void getData() {
        try{
            tableClubMembers.getItems().clear();
            Connection connection = dcm.getConnection();
            ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM mitglieder");

            while(rs.next()){
                clubmembers.add(new ClubMember(rs.getString("vorname"), rs.getString("nachname"), rs.getInt("alter")));
            }
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        colVorname.setCellValueFactory(new PropertyValueFactory<>("Vorname"));
        colNachname.setCellValueFactory(new PropertyValueFactory<>("Nachname"));
        colAlter.setCellValueFactory(new PropertyValueFactory<>("Alter"));

        tableClubMembers.setItems(clubmembers);
    }
}
