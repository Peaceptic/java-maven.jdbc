package application;

public class ClubMember {
    private String Vorname;
    private String Nachname;
    private int Alter;
    
    public ClubMember(){
        Vorname = "undefined";
        Nachname = "undefined";
        Alter = 0;
    }

    public ClubMember(String firstname, String lastname, int age){
        Vorname = firstname;
        Nachname = lastname;
        Alter = age;
    }

    public void setVorname(String firstname){
        Vorname = firstname;
    }

    public String getVorname(){
        return Vorname;
    }

    public void setNachname(String lastname){
        Nachname = lastname;
    }

    public String getNachname(){
        return Nachname;
    }

    public void setAlter(int age){
        Alter = age;
    }

    public int getAlter(){
        return Alter;
    }
}
